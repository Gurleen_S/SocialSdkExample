//
//  InteractiveFolderTableViewCell.swift
//  SDK
//
//  Created by Gurleen Singh on 04/01/23.
//  Copyright © 2023 Gurleen Singh. All rights reserved.
//

import UIKit
import ShipBookSDK

class InteractiveFolderTableViewCell: UITableViewCell {

    @IBOutlet weak var viewCellBackground: UIView!
    @IBOutlet weak var imgPhotoPreview: UIImageView!
    
    @IBOutlet weak var lblAlbumName: UILabel!
    @IBOutlet weak var lblImgCount: UILabel!
    @IBOutlet weak var imgViewDot: UIImageView!
    
    var representedAssetIdentifier = String()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
