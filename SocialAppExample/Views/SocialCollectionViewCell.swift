//
//  SocialCollectionViewCell.swift
//  SDK
//
//  Created by Gurleen Singh on 04/01/23.
//  Copyright © 2023 Gurleen Singh. All rights reserved.
//




import UIKit
import ShipBookSDK

class SocialCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var socialPhotosLoader: UIActivityIndicatorView!

    override func awakeFromNib() {
        
    }
    
}
