//
//  AppDelegate.swift
//  SocialAppExample
//
//  Created by iOS Developer on 05/01/23.
//

import UIKit
import FBSDKCoreKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let signInConfig = GIDConfiguration.init(clientID: "775198682777-dg8p8gg76ru4jr4i2ndoc5eau5h6md5u.apps.googleusercontent.com")

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        FBSDKCoreKit.ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

        // Override point for customization after application launch.
        return true
    }


}

