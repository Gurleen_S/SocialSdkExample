//
//  SocialViewController.swift
//  SocialAppExample
//
//  Created by iOS Developer on 09/01/23.
//

import UIKit
import AlamofireImage
class SocialViewController: UIViewController {
    
    var array_Social_PhotoUrl = [String]()
    @IBOutlet weak var gridCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.gridCollectionView?.collectionViewLayout = layout
        self.gridCollectionView?.isPagingEnabled = true
        // Do any additional setup after loading the view.
    }
}

extension SocialViewController : UICollectionViewDelegate, UICollectionViewDataSource
{
    
    // MARK: - CollectionView Datasource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_Social_PhotoUrl.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SocialCollectionViewCell.self), for: indexPath) as? SocialCollectionViewCell
        else { fatalError("unexpected cell in collection view") }
        cell.imageView.image = nil
        cell.socialPhotosLoader.startAnimating()
        
        if array_Social_PhotoUrl.count > 0 {
            let url = array_Social_PhotoUrl[indexPath.row]
            cell.imageView.af.setImage(
                withURL: URL(string:url)!,
                imageTransition: .crossDissolve(0.2),
                completion: { response in
                    if response.value != nil {
                        cell.socialPhotosLoader.stopAnimating()
                    }
                }
            )
        }
        
        return cell
    }
    
    // MARK: - CollectionView Layout Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((collectionView.frame.width - 15) / 2) // 15 because of paddings
        return CGSize(width: width, height: 200)
    }
    
    // MARK: - CollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
