//
//  ViewController.swift
//  SDK
//
//  Created by Gurleen Singh on 04/01/23.
//  Copyright © 2023 Gurleen Singh. All rights reserved.
//


import UIKit
import AlamofireImage
import SocialSDK

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonFacebook: UIButton!
    @IBOutlet weak var buttonGooglePhotos: UIButton!
    @IBOutlet weak var buttonInstagram: UIButton!
    
    let social = SocialSDK()
    var senderId : Int? = nil
    var savedFbPhotosArray = [FacebookModels]()
    var savedGooglePhotosArray = [GooglePhotosModels]()
    var instagramPhotosArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        resetNilValues()
        checkIfConnected()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.getInstagramLogin() == true, let photos = UserDefaults.getInstagramPhotos(), photos.count != 0 {
            instagramPhotosArray = photos
            self.resetTableView()
        }
        self.checkIfConnected()
    }

    @IBAction func actionInstagram(_ sender: UIButton) {
        if UserDefaults.getInstagramLogin() == true, let photos = UserDefaults.getInstagramPhotos(), photos.count != 0 {
            self.resetTableView()
            self.reloadTableView(sender: sender)
            let _viewController = self.storyboard?.instantiateViewController(withIdentifier: "SocialViewController") as! SocialViewController
            _viewController.array_Social_PhotoUrl = self.instagramPhotosArray
            self.navigationController?.pushViewController(_viewController, animated: true)
        } else {
            DispatchQueue.main.async {
                self.social.socialMediaInstagram(viewController: self)
            }
        }
    }
     
    @IBAction func actionGoogle(_ sender: UIButton) {
        self.resetTableView()
        social.socialMediaGooglePhotos(viewController: self) {googlePhotos in
            self.checkIfConnected()
            self.savedGooglePhotosArray = googlePhotos
            self.reloadTableView(sender: sender)
        }
    }
    
    @IBAction func actionFacebook(_ sender: UIButton) {
        self.resetTableView()
        social.fetchSocialMediaFacebook() { arrayFBAlbum in
            self.checkIfConnected()
            self.savedFbPhotosArray = arrayFBAlbum
            self.reloadTableView(sender: sender)
        }
    }
}

extension ViewController {
    func reloadTableView(sender:UIButton) {
        DispatchQueue.main.async {
            self.senderId = sender.tag
            self.tableView.reloadData()
        }
    }
    
    func resetTableView(){
        senderId = 0
        self.tableView.reloadData()
        self.savedGooglePhotosArray.removeAll()
        self.savedGooglePhotosArray.removeAll()
    }
    
    func checkIfConnected() {
        if UserDefaults.getFacebookLogin() == true {
            self.buttonFacebook.backgroundColor = UIColor.green
        }
        
        if UserDefaults.getGoogleLogin() == true {
            self.buttonGooglePhotos.backgroundColor = UIColor.green
        }
        
        if UserDefaults.getInstagramLogin() == true, let photos = UserDefaults.getInstagramPhotos(), photos.count != 0 {
            self.buttonInstagram.backgroundColor = UIColor.green
        }
    }
    
    func resetNilValues(){
        if UserDefaults.getFacebookLogin() == nil {
            UserDefaults.setFacebookLogin(false)
        }
        if UserDefaults.getInstagramLogin() == nil {
            UserDefaults.setInstagramLogin(false)
        }
        if UserDefaults.getGoogleLogin() == nil {
            UserDefaults.setGoogleLogin(false)
        }
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if senderId == 10 {
            return savedFbPhotosArray.count
        } else if senderId == 11 {
            return savedGooglePhotosArray.count
        } else if senderId == 12 {
            //return instagramPhotosArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : InteractiveFolderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InteractiveFolderTableViewCell", for: indexPath) as! InteractiveFolderTableViewCell
        if senderId == 10 {
            let album = savedFbPhotosArray[indexPath.row]
            cell.lblAlbumName.text = album.stringAlbumName
            cell.lblImgCount.text = "\(album.intPhotosCount)"
            let url = album.stringAlbumThumbnailUrl
            cell.imgPhotoPreview.af.setImage(
                withURL: URL(string: url)!,
                imageTransition: .crossDissolve(0.2),
                completion: nil)
            
            cell.imgViewDot.isHidden = false
            cell.viewCellBackground.backgroundColor = UIColor(red:0.21, green:0.24, blue:0.78, alpha:1)
        } else if senderId == 11 {
            let album = savedGooglePhotosArray[indexPath.row]
            cell.lblAlbumName?.text = album.title
            if indexPath.row != 0 {
                cell.lblImgCount?.text = "\(album.mediaCount)"
            }else {
                cell.lblImgCount?.text = ""
            }
            
             let url = album.coverPhoto
            if url != "" {
            cell.imgPhotoPreview.af.setImage(withURL: URL(string: url)!)
            }
            cell.imgViewDot.isHidden = false
            cell.viewCellBackground.backgroundColor = UIColor(red:0.21, green:0.24, blue:0.78, alpha:1)

        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if senderId == 10 {
            let album = savedFbPhotosArray[indexPath.row]
            social.getFacebookPhotosFromID(viewController: self, album: album) {Result,pageToken,arrayPhoto in
                if Result == true {
                    let _viewController = self.storyboard?.instantiateViewController(withIdentifier: "SocialViewController") as! SocialViewController
                    _viewController.array_Social_PhotoUrl = arrayPhoto
                    self.navigationController?.pushViewController(_viewController, animated: true)
                }
            }
        } else if senderId == 11 {
            let album = savedGooglePhotosArray[indexPath.row]
            guard let accessToken = UserDefaults.getGoogleAccessToken() else {
                return
            }
            social.getGooglePhotosFromID(viewController: self, selectedAlbumId: album.googleAlbumId, accessToken: accessToken, nextPageToken: "") {googlePhotosArray,photos,nextPageToken in
                let _viewController = self.storyboard?.instantiateViewController(withIdentifier: "SocialViewController") as! SocialViewController
                _viewController.array_Social_PhotoUrl = googlePhotosArray
                self.navigationController?.pushViewController(_viewController, animated: true)

            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
